#!/usr/bin/env python
# -*- coding: utf-8 -*-


PROJECT_ID = 'carbon-compound-223319'
ZONE = 'us-east4-b'
OPERATION_ID = 'operation-1544273292498-8ccc3f9d'

mocked_operation = {
    'endTime': '2018-12-08T12:50:30.800590052Z',
    'name': OPERATION_ID,
    'operationType': 'CREATE_CLUSTER',
    'selfLink': f'https://container.googleapis.com/v1/projects/705163204618/zones/{ZONE}/operations/{OPERATION_ID}',
    'startTime': '2018-12-08T12:48:12.498003165Z',
    'status': 'DONE',
    'targetLink': f'https://container.googleapis.com/v1/projects/705163204618/zones/{ZONE}/clusters/cattlelizers',
    'zone': ZONE
}

mocked_cluster = {
    'addonsConfig': {'horizontalPodAutoscaling': {},
                     'httpLoadBalancing': {},
                     'kubernetesDashboard': {},
                     'networkPolicyConfig': {'disabled': True}},
    'clusterIpv4Cidr': '10.24.0.0/14',
    'createTime': '2018-12-08T12:48:10+00:00',
    'currentMasterVersion': '1.10.9-gke.5',
    'currentNodeCount': 3,
    'currentNodeVersion': '1.10.9-gke.5',
    'description': 'Cattlelizers test cluster',
    'endpoint': '35.236.220.139',
    'initialClusterVersion': '1.10.9-gke.5',
    'initialNodeCount': 3,
    'instanceGroupUrls': [
        f'https://www.googleapis.com/compute/v1/projects/{PROJECT_ID}/zones/{ZONE}/instanceGroupManagers/gke-cattlelizers-default-pool-51bcecdf-grp'],
    'labelFingerprint': 'a9dc16a7',
    'legacyAbac': {},
    'location': ZONE,
    'locations': [ZONE],
    'loggingService': 'logging.googleapis.com',
    'masterAuth': {
        'clientCertificate': 'LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUMyakNDQWNLZ0F3SUJBZ0lRUTNaMFM2Y0dkZDd6WUtXNEZPSmdTVEFOQmdrcWhraUc5dzBCQVFzRkFEQXYKTVMwd0t3WURWUVFERXlReVpUUXpPVGhpWkMxalpURXlMVFF5TW1FdE9UQTRPQzFrWmpOa05ERXlZakU0WXprdwpIaGNOTVRneE1qQTRNVEkwT0RFd1doY05Nak14TWpBM01USTBPREV3V2pBUk1ROHdEUVlEVlFRREV3WmpiR2xsCmJuUXdnZ0VpTUEwR0NTcUdTSWIzRFFFQkFRVUFBNElCRHdBd2dnRUtBb0lCQVFDekZyV0ZDT0ZDTFB6YjNrc3kKdzREbE12T3lHc2JpbjFPRkp0ZGZWR0syVUR4WTkyL0hRRHNQYk9JcDhMZ3N4d2Z6bHNtRU5lUlhRakk3bU9jMgpUUHdzQ1E1U3JtcjkvYTFEUEF3TzlPWm1vaVNYUVJHcEMzWFVuWmx5dmk2TFlBMlpKNUVXaDNqQjhUM09TLzQ4CkVtN0JHN1BXc3libTVBZGpNc0xTc2FSanNmK2tCYVZpWS9oT2ZPOUFzb3RjekhZekRmSFJmdGhrU040VklKWEgKNzd0RjBPWitkSGNWRi82dHR6dXdIalNVM1dpVTJYQS8xenFyUjRlb0lWaVlWemtuODFxN0NGMGl1SHRKRmJNcQpTdGtsbEZvUy8rV21WKzFRUmhqK3I2clhxVHhUTGpRbW9QNVFjUitqdzN2eTNoR0ZPK09YWEpHN1FaVTNFcG9KCmM2VmJBZ01CQUFHakVEQU9NQXdHQTFVZEV3RUIvd1FDTUFBd0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFBVFAKZmtlUFRnTHZXc2ZzZ0FwZmV2dVZndnViUGRWZDJ6dXpkUUtHSHlLUW45WmloY2hFTzRqeUNUbmdkdG1mQzNoVAo1QUQzckoxZWl3Z2JkR0gzcFluRmpoYndZRDJUdm5HdXRDWXZiKzYrWnl0NWhoalZodnVwWFByY29aWWNZK2JnCmFVb3AyRkJFOHMrNmdmVG03U01SaTRKYUUySE9ybkFvdkpmMDVyVkpIcmUzY3lPVzUraU1CbzVNUG4zeTlDNEMKc2xSMG03NGNiY1lPenpHNGdwenRySWYwM3h2S3pHVmxjTXZtN2p4OUp6YTdjT1ZVV1R4NGJyVHJiUGFweC96cQpnSlVZUUtNRXIzcTduRktYT1FRNXlOY2gyUVdrVVBTcUxGN1pVQWlHSkpTaG45ZXhnaktPWStTL202SXVoQit2CkFhUG9TcUZpRGVUcXFTeFFZVW89Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K',
        'clientKey': 'LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQpNSUlFcEFJQkFBS0NBUUVBc3hhMWhRamhRaXo4Mjk1TE1zT0E1VEx6c2hyRzRwOVRoU2JYWDFSaXRsQThXUGR2CngwQTdEMnppS2ZDNExNY0g4NWJKaERYa1YwSXlPNWpuTmt6OExBa09VcTVxL2YydFF6d01EdlRtWnFJa2wwRVIKcVF0MTFKMlpjcjR1aTJBTm1TZVJGb2Q0d2ZFOXprditQQkp1d1J1ejFyTW01dVFIWXpMQzByR2tZN0gvcEFXbApZbVA0VG56dlFMS0xYTXgyTXczeDBYN1laRWplRlNDVngrKzdSZERtZm5SM0ZSZityYmM3c0I0MGxOMW9sTmx3ClA5YzZxMGVIcUNGWW1GYzVKL05hdXdoZElyaDdTUld6S2tyWkpaUmFFdi9scGxmdFVFWVkvcStxMTZrOFV5NDAKSnFEK1VIRWZvOE43OHQ0UmhUdmpsMXlSdTBHVk54S2FDWE9sV3dJREFRQUJBb0lCQUJDNVJpb3VNaHJqNkpqLwpvVFlVVzYwTG5RWTh5ZVhQeXhUY0U3Q3JTS3lOd1pUdFJscnMvSmJzTEJLT2tDUmVVYmZGbFB2cnJHOFpsMmRKCldBNFNFbHBxWGhRS2hhTkNWVE82SDhZbHlINDlGZjJwN2VSRFZiV05JZzg5QTB1d3BKUnd4WE85SXVHNWpYUCsKVkxkM1lUQlhUZXYydnVOQTJBL0RaYjRYS0tyR2hCaXhkWjZIeVBibmtUOUxiV3d2WEZIMVFGaXZWY1NrY1BDeQpjamV3UkU3MWcrL2NBM2Nta3QrUVBLTHptVmZZL09tZSsvd3c3MG9iV0htU05BakwyVUNjS3dnUFBNVDZvbGhSCmcxZ2loaFVVc3hMWCszSWFpaTY4WEVjVTd2a3B3SE9xNklkZERhV24rOGlZWVJKa3lIbC92UzNGcktuaDBMWXIKcUVIZERBRUNnWUVBOTFhWjJIQVZ3d3EyZjZ4RG1BUUJ4OVdBQVhwNEtHRjlLamZOdGdMOHpvRzBuUEpPNGZDUApSQW9xdi9zTGJmbXpBdm5SSDZQYnhFNGlxeWVDRGZjRVpIWDVWM2YvMHYwbmM4OFBYamE0WFhIQjNUTEtwa3dYCk1tSWRjRkVWYmdLOUc2OTZsbEc3UHRBRWNyMUh3Z3E5Y0h2UXo0c2E4NGFTVCtuS0c5QjVseHNDZ1lFQXVWdy8KWFMxUGtWaUFKNjRCVTJlbWhOQ2E1TGEvSTZGU3FmSHVCbitURE95R2xQT05oRWRBWEs3TDQ2V3JodUZYdFhtOQpkSjRlZERxUTE1eWV1TytpWTd3ZnJMOXN1dFB3YmhaNW9TNDVOWEhJRzRPNEtka3FkdkhtbTlGYnZiOTVuT1luCmo2NkQwc3duQW90SmRkdlY0bDBSeU51ZE5TV3dnREwvbTNDanpzRUNnWUVBNTFrRmhjRXhXY0lIZWR3elRWbFMKSVF5K01xUFBDRlpIc05tSmZKbkd4WkhrMG5pQytDcitsbkc4RXQ1S2pvOWt0WC9kNzI1Zy9Yam9USmNqdXRRMgpNNUYxckM3K0J1ZnJveE1tR01XdUpSbG1iL1EyUU40alJHa3BpQWtQdGxxeGI5ZnZIZk5NNVMvRk9WMTZaVEdPCmlFd0h3RmtvMkdnaktzUk8yejFndmNrQ2dZQVlYeW14cmFoOW10NVcyVWt4VnpxTTBnVGJ0NitRRnNFUXVVK3IKcVJDRHJZSFFpZWw4a0FYRmFmSkMvMkI3UHB2U3I2d2Fia1BnMG1hREF3UFhteUtGN0pGWm5GM0dmdUZwQ0NsNwo2T0Q2eHZnaHJnTUQ5WnJnd3RPb2Z6R0oxR2hBR2Y3dnBHbU95T3NFS1o1Q0VlS2dmYzQ5MDZwcTk0YXFoSW9aClBRazh3UUtCZ1FDaTNpeDVKR1lyR3U2MnZERmV3TUZKc2VpME81cUtyUlZTTkl0MTBCb3FpMm9mdFoxMCt4TXEKejJYYjJnamxGTEVic0FVT3lRZTBNUWIxVDRHYVIxRjVTT2MyWGxKQTJZd2F4ZWVObnYvRGJ0eHQzUGVTUGUwSgpydW1HN1hVS0dvYVk4d2NNb1pGYkFRc3hDT3JzUEx1eEtvUXAxbzU5bzhOWkYzQXAxTEE1aUE9PQotLS0tLUVORCBSU0EgUFJJVkFURSBLRVktLS0tLQo=',
        'clusterCaCertificate': 'LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSURDekNDQWZPZ0F3SUJBZ0lRZUVMZ0YwMzJyMHZIQkZUbXdrd2pqakFOQmdrcWhraUc5dzBCQVFzRkFEQXYKTVMwd0t3WURWUVFERXlReVpUUXpPVGhpWkMxalpURXlMVFF5TW1FdE9UQTRPQzFrWmpOa05ERXlZakU0WXprdwpIaGNOTVRneE1qQTRNVEUwT0RFd1doY05Nak14TWpBM01USTBPREV3V2pBdk1TMHdLd1lEVlFRREV5UXlaVFF6Ck9UaGlaQzFqWlRFeUxUUXlNbUV0T1RBNE9DMWtaak5rTkRFeVlqRTRZemt3Z2dFaU1BMEdDU3FHU0liM0RRRUIKQVFVQUE0SUJEd0F3Z2dFS0FvSUJBUUMzUWJsL3lJdlUxLzFPck5wck8wVVRDSmVZa2pPTUtnZzdtd0ZPclhsagpTd2ozZU1GSFBtSkNoS2d1NWpqVEMzcmtRNFhPZjJqbFFWelBYZkZIMUc3NWFrU1g5bEExQm9KbXpJbHdCd1hzCjJRTHk5Ujl1RXJsVURzK05SNTRLVUpCemlnWjkyN2xHWUlZTUc0Snp3cFgvYlI3VEJqUCs3aTE0aEhJcUZGQk8KT1pxTDJGTGFFNjYvUStWaVlIM1J5S2VJQ3JCbWxTdjdCaGtocTZzUWJhcE9XSHlzRnltSng3a0QxeVBVM25mWQpQYWFWWVZaeUYyeE5yYnUyVUd4N21RN0FWR085aEZaTmQyNFdYWkx3MkdQelBzR293UzR5M3JzdFVpMWszN2NuCjZ1RVBaajlsOHJ3TUpaZ2NRTnlKckZUOExwV3Y0OHMyVzY5dmNINkMrQlJsQWdNQkFBR2pJekFoTUE0R0ExVWQKRHdFQi93UUVBd0lDQkRBUEJnTlZIUk1CQWY4RUJUQURBUUgvTUEwR0NTcUdTSWIzRFFFQkN3VUFBNElCQVFCVgpYT0Q5M09HbHI2N1FGS2FkMUpMTjJWN0FPMUdYOWZIRU56MzVYY2xoKzVQcDV6MFl0b041cFhreDZla0RmZ1BaCm9XaHJTOVZKckxqYStZUE1zc2hKQjdtd0ZDZWMvYTh0Q1dDaFpWa1JWMU9UeXNOa1hPMDNuY2Q4enZlQkRGN1YKZGlyMDc2MVRFOCtOU202dHBoMU0zM3NwVklRMHk0LzBwQTlHSVd6eU1NWGFiQmhjR3ZveEF1dnVEVUlQUEwvSgpDWW5wN3duTTJuVkVRUWFuNVpaSkVMOXhIdDFyNStOMVRaTGhoZVVEL1lURXRhUXlkbStyaG52TlhyOW51ZmFvClFrUmpsdWtya295d3Faa3JDMkxkdEw0NGw3Z1hibUJTVkpMWVBNRVB4eWJFM1dnQVZKUENtMHQ2ZW9wdXBqSzYKMnV6cnB5ajB0U24vT1NPQUlzR0QKLS0tLS1FTkQgQ0VSVElGSUNBVEUtLS0tLQo=',
        'password': 'khoYYlGsxRh8FmBb',
        'username': 'admin'},
    'monitoringService': 'monitoring.googleapis.com',
    'name': 'cattlelizers',
    'network': 'default',
    'networkConfig': {'network': f'projects/{PROJECT_ID}/global/networks/default',
                      'subnetwork': f'projects/{PROJECT_ID}/regions/us-east4/subnetworks/default'},
    'nodeConfig': {'diskSizeGb': 100,
                   'diskType': 'pd-standard',
                   'imageType': 'COS',
                   'machineType': 'n1-standard-1',
                   'oauthScopes': ['https://www.googleapis.com/auth/logging.write',
                                   'https://www.googleapis.com/auth/monitoring'],
                   'serviceAccount': 'default'},
    'nodeIpv4CidrSize': 24,
    'nodePools': [{'config': {'diskSizeGb': 100,
                              'diskType': 'pd-standard',
                              'imageType': 'COS',
                              'machineType': 'n1-standard-1',
                              'oauthScopes': ['https://www.googleapis.com/auth/logging.write',
                                              'https://www.googleapis.com/auth/monitoring'],
                              'serviceAccount': 'default'},
                   'initialNodeCount': 3,
                   'instanceGroupUrls': [
                       f'https://www.googleapis.com/compute/v1/projects/{PROJECT_ID}/zones/{ZONE}/instanceGroupManagers/gke-cattlelizers-default-pool-51bcecdf-grp'],
                   'management': {},
                   'name': 'default-pool',
                   'selfLink': f'https://container.googleapis.com/v1/projects/{PROJECT_ID}/zones/{ZONE}/clusters/cattlelizers/nodePools/default-pool',
                   'status': 'RUNNING',
                   'version': '1.10.9-gke.5'}],
    'selfLink': f'https://container.googleapis.com/v1/projects/{PROJECT_ID}/zones/{ZONE}/clusters/cattlelizers',
    'servicesIpv4Cidr': '10.27.240.0/20',
    'status': 'RUNNING',
    'subnetwork': 'default',
    'zone': ZONE
}


class GoogleAPIMock(object):
    """ Mock for Google API """

    def __init__(self, service, version, credentials=None):
        """ Class constructor """
        pass

    class Projects(object):
        """ Projects API object mock """

        class Zones(object):
            """ Zones API object mock """

            class Operations(object):
                """ Operations API object mock """

                class GetRequestMock(object):
                    """ Get request mock """

                    def execute(self):
                        return mocked_operation

                def get(self, projectId=None, zone=None, operationId=None):
                    return self.GetRequestMock()

            class Clusters(object):
                """ Operations API object mock """

                class GetRequestMock(object):
                    """ GET request mock """

                    def execute(self):
                        return mocked_cluster

                class CreateRequestMock(object):
                    """ Create request mock """

                    def execute(self):
                        return mocked_operation

                def create(self, projectId=None, zone=None, body=None):
                    return self.CreateRequestMock()

                def get(self, projectId=None, zone=None, clusterId=None):
                    return self.GetRequestMock()

            def operations(self):
                return self.Operations()

            def clusters(self):
                return self.Clusters()

        def zones(self):
            return self.Zones()

    def projects(self):
        return self.Projects()


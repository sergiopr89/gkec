#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest
from tests.google_mocks import *
from gkec import Gkec, OperationAbortedError, OperationTimedoutError,\
    OperationStatusUnknownError


def test_create_cluster(mocker):
    """ Test for create cluster method """
    mocker.patch.object(Gkec, '_create_service', return_value=GoogleAPIMock('', ''))
    client = Gkec('some_auth_file.json')
    cluster = client.create_cluster(PROJECT_ID, ZONE, 'Some')
    assert cluster['zone'] == ZONE


def test_create_cluster_async(mocker):
    """ Test for create cluster async method """
    mocker.patch.object(Gkec, '_create_service', return_value=GoogleAPIMock('', ''))
    client = Gkec('some_auth_file.json')
    operation = client.create_cluster_async(PROJECT_ID, ZONE, 'Some')
    assert operation == OPERATION_ID


def test_check_operation(mocker):
    """ Test for check operation method """
    mocker.patch.object(Gkec, '_create_service', return_value=GoogleAPIMock('', ''))
    client = Gkec('some_auth_file.json')
    status = client.check_operation(PROJECT_ID, ZONE, OPERATION_ID)
    assert status == 'DONE'


def test_wait_operation_done(mocker):
    """ Test for wait operation done method """
    mocker.patch.object(Gkec, '_create_service', return_value=GoogleAPIMock('', ''))
    client = Gkec('some_auth_file.json')
    status = client.wait_operation_done(PROJECT_ID, ZONE, OPERATION_ID, 10, 1)
    assert status == 'DONE'

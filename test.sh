#!/bin/bash

pytest --cov=gkec tests/
rm .coverage
rm -rf .pytest_cache
